import cards.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Card 1:");
        BankCard card1 = new DebitCard();
        System.out.println(card1.getBalance());
        System.out.println(card1.pay(10));
        card1.replenish(100);
        System.out.println(card1.pay(50));
        System.out.println(card1.pay(60));
        System.out.println(card1.getFunds());
        System.out.println(card1.pay(10));
        System.out.println(card1.getFunds());

        System.out.println("Card 2:");
        CreditCard card2 = new CreditCard(10000);
        System.out.println(card2.getFunds());
        card2.replenish(5000);
        System.out.println(card2.getFunds());
        System.out.println(card2.getBalance());
        card2.pay(5000);
        System.out.println(card2.getFunds());
        card2.pay(3000);
        System.out.println(card2.getFunds());
        card2.replenish(2000);
        System.out.println(card2.getFunds());
        card2.replenish(2000);

        System.out.println("Card 3:");
        CashbackDebitCard card3 = new CashbackDebitCard();
        card3.replenish(10000);
        card3.pay(1000);
        System.out.println(card3.getFunds());
        card3.applyBonus(); // раз в месяц
        System.out.println(card3.getFunds());


        System.out.println("Card 4:");
        CreditCard card4 = new BonusCreditCard(10000);
        System.out.println(card4.getFunds());
        card4.replenish(5000000);
        System.out.println(card4.getFunds());
        System.out.println(card4.getBalance());
        card4.pay(5000000);
        System.out.println(card4.getFunds());
        card4.pay(3000);
        System.out.println(card4.getFunds());
        card4.replenish(2000);
        System.out.println(card4.getFunds());
        card4.replenish(2000);

    }
}