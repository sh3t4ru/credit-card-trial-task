package cards;

public class BonusCreditCard extends CreditCard {

    public BonusCreditCard(long creditLimit) {
        super(creditLimit);
    }

    @Override
    public void replenish(long amount) {
        funds.setBalance(funds.getBalance() + (long) (amount * 0.00005));
        super.replenish(amount);
    }

    @Override
    public boolean pay(long amount) {
        return super.pay(amount);
    }

    @Override
    public CreditFunds getFunds() {
        return super.getFunds();
    }

    @Override
    public long getBalance() {
        return super.getBalance();
    }

}
