package cards;

public class DebitCard extends BankCard {

    @Override
    public void replenish(long amount) {
        funds.setBalance(funds.getBalance() + amount);
    }

    @Override
    public boolean pay(long amount) {
        if (amount > funds.getBalance()) {
            return false;
        }
        funds.setBalance(funds.getBalance() - amount);
        return true;
    }

    @Override
    public Funds getFunds() {
        return funds;
    }

    @Override
    public long getBalance() {
        return funds.getBalance();
    }

}
