package cards;

public class CreditCard extends BankCard {

    protected final CreditFunds funds;

    public CreditCard(long creditLimit) {
        this.funds = new CreditFunds(0, creditLimit);
    }

    @Override
    public void replenish(long amount) {
        if (funds.getCreditBalance() + amount < 0) {
            funds.setCreditBalance(funds.getCreditBalance() + amount);
            amount = 0;
        } else {
            amount += funds.getCreditBalance();
            funds.setCreditBalance(0);
        }
        funds.setBalance(funds.getBalance() + amount);
    }

    @Override
    public boolean pay(long amount) {
        if (funds.getBalance() + funds.getCreditLimit() + funds.getCreditBalance() < amount) {
            return false;
        }
        if (amount < funds.getBalance()) {
            funds.setBalance(funds.getBalance() - amount);
            amount = 0;
        } else {
            amount -= funds.getBalance();
            funds.setBalance(0);
        }
        funds.setCreditBalance(funds.getCreditBalance() - amount);
        return true;
    }

    @Override
    public CreditFunds getFunds() {
        return funds;
    }

    @Override
    public long getBalance() {
        return funds.getBalance();
    }

    protected static class CreditFunds extends Funds {
        protected long creditLimit;
        protected long creditBalance;

        protected CreditFunds(long balance, long creditLimit) {
            super(balance);
            this.creditLimit = creditLimit;
            this.creditBalance = 0;
        }

        public long getCreditLimit() {
            return creditLimit;
        }

        public long getCreditBalance() {
            return creditBalance;
        }

        protected void setCreditBalance(long creditBalance) {
            this.creditBalance = creditBalance;
        }

        @Override
        public String toString() {
            return "CreditFunds{" +
                    "balance=" + balance +
                    ", creditLimit=" + creditLimit +
                    ", creditBalance=" + creditBalance +
                    '}';
        }
    }

}
