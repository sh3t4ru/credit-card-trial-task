package cards;

// вся эта система не является потокобезопасной
public abstract class BankCard {
    protected final Funds funds; // в копейках/центах/минимальной доле валюты

    public BankCard() {
        this.funds = new Funds(0);
    }

    public abstract void replenish(long amount); // необходима валидация что сумма неотрицательна

    public abstract boolean pay(long amount); // необходима валидация что сумма неотрицательна

    public abstract Funds getFunds();

    public abstract long getBalance();

    protected static class Funds {
        protected long balance;

        protected Funds(long balance) {
            this.balance = balance;
        }

        public long getBalance() {
            return balance;
        }

        protected void setBalance(long balance) {
            this.balance = balance;
        }

        @Override
        public String toString() {
            return "Funds{" +
                    "balance=" + balance +
                    '}';
        }
    }
}
