package cards;

public class CashbackDebitCard extends DebitCard {

    private final CashbackFunds funds;

    public CashbackDebitCard() {
        this.funds = new CashbackFunds(0);
    }


    @Override
    public void replenish(long amount) {
        funds.setBalance(funds.getBalance() + amount);
    }

    @Override
    public boolean pay(long amount) {
        if (amount > funds.getBalance()) {
            return false;
        }
        funds.setBalance(funds.getBalance() - amount);
        funds.setBonus(funds.getBonus() + (long) (amount * 0.01));
        return true;
    }

    @Override
    public CashbackFunds getFunds() {
        return funds;
    }

    @Override
    public long getBalance() {
        return funds.getBalance();
    }

    public void applyBonus() { // ежемесечная джоба должна быть
        funds.setBalance(funds.getBalance() + funds.getBonus());
        funds.setBonus(0);
    }
    protected static class CashbackFunds extends Funds {
        protected long bonus;

        protected CashbackFunds(long balance) {
            super(balance);
        }

        public long getBonus() {
            return bonus;
        }

        protected void setBonus(long bonus) {
            this.bonus = bonus;
        }

        @Override
        public String toString() {
            return "CashbackFunds{" +
                    "bonus=" + bonus +
                    ", balance=" + balance +
                    '}';
        }
    }
}
